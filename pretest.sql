--
-- PostgreSQL database dump
--

-- Dumped from database version 11.14
-- Dumped by pg_dump version 11.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Kelas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Kelas" (
    id integer NOT NULL,
    nama character varying(255),
    id_sekolah integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Kelas" OWNER TO postgres;

--
-- Name: Kelas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Kelas_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Kelas_id_seq" OWNER TO postgres;

--
-- Name: Kelas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Kelas_id_seq" OWNED BY public."Kelas".id;


--
-- Name: Sekolahs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Sekolahs" (
    id integer NOT NULL,
    nama character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Sekolahs" OWNER TO postgres;

--
-- Name: Sekolahs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Sekolahs_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Sekolahs_id_seq" OWNER TO postgres;

--
-- Name: Sekolahs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Sekolahs_id_seq" OWNED BY public."Sekolahs".id;


--
-- Name: SequelizeMeta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SequelizeMeta" (
    name character varying(255) NOT NULL
);


ALTER TABLE public."SequelizeMeta" OWNER TO postgres;

--
-- Name: Siswas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Siswas" (
    id integer NOT NULL,
    nama character varying(255),
    id_kelas integer,
    id_sekolah integer,
    nilai double precision,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Siswas" OWNER TO postgres;

--
-- Name: Siswas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Siswas_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Siswas_id_seq" OWNER TO postgres;

--
-- Name: Siswas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Siswas_id_seq" OWNED BY public."Siswas".id;


--
-- Name: Kelas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Kelas" ALTER COLUMN id SET DEFAULT nextval('public."Kelas_id_seq"'::regclass);


--
-- Name: Sekolahs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Sekolahs" ALTER COLUMN id SET DEFAULT nextval('public."Sekolahs_id_seq"'::regclass);


--
-- Name: Siswas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Siswas" ALTER COLUMN id SET DEFAULT nextval('public."Siswas_id_seq"'::regclass);


--
-- Data for Name: Kelas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Kelas" (id, nama, id_sekolah, "createdAt", "updatedAt") FROM stdin;
1	Wisozk - Hettinger	1	2022-03-10 11:20:53.577+07	2022-03-10 11:20:53.577+07
2	Hamill - Kunze	1	2022-03-10 11:20:53.597+07	2022-03-10 11:20:53.597+07
3	Jerde and Sons	1	2022-03-10 11:20:53.599+07	2022-03-10 11:20:53.599+07
4	Kreiger - Heaney	1	2022-03-10 11:20:53.6+07	2022-03-10 11:20:53.6+07
5	Paucek LLC	1	2022-03-10 11:20:53.601+07	2022-03-10 11:20:53.601+07
6	Yost, Kling and Lowe	1	2022-03-10 11:20:53.602+07	2022-03-10 11:20:53.602+07
7	Reilly, Littel and Rosenbaum	1	2022-03-10 11:20:53.604+07	2022-03-10 11:20:53.604+07
8	Kutch, Cummings and Johnson	1	2022-03-10 11:20:53.605+07	2022-03-10 11:20:53.605+07
9	Rohan - Hand	1	2022-03-10 11:20:53.606+07	2022-03-10 11:20:53.606+07
10	Hickle, Deckow and Stanton	1	2022-03-10 11:20:53.607+07	2022-03-10 11:20:53.607+07
11	Romaguera Group	2	2022-03-10 11:20:53.608+07	2022-03-10 11:20:53.608+07
12	Flatley Group	2	2022-03-10 11:20:53.609+07	2022-03-10 11:20:53.609+07
13	Purdy - Zboncak	2	2022-03-10 11:20:53.61+07	2022-03-10 11:20:53.61+07
14	Nader - Kessler	2	2022-03-10 11:20:53.611+07	2022-03-10 11:20:53.611+07
15	Steuber - Lesch	2	2022-03-10 11:20:53.612+07	2022-03-10 11:20:53.612+07
16	Altenwerth - Konopelski	2	2022-03-10 11:20:53.613+07	2022-03-10 11:20:53.613+07
17	McClure, Parisian and Roberts	2	2022-03-10 11:20:53.613+07	2022-03-10 11:20:53.613+07
18	Heathcote - Graham	2	2022-03-10 11:20:53.618+07	2022-03-10 11:20:53.618+07
19	Stark, O'Hara and Rippin	2	2022-03-10 11:20:53.618+07	2022-03-10 11:20:53.618+07
20	Grant - Hayes	2	2022-03-10 11:20:53.619+07	2022-03-10 11:20:53.619+07
21	Rohan - Dickinson	3	2022-03-10 11:20:53.62+07	2022-03-10 11:20:53.62+07
22	Schiller LLC	3	2022-03-10 11:20:53.621+07	2022-03-10 11:20:53.621+07
23	Boehm, Torphy and Feil	3	2022-03-10 11:20:53.621+07	2022-03-10 11:20:53.621+07
24	Bechtelar and Sons	3	2022-03-10 11:20:53.622+07	2022-03-10 11:20:53.622+07
25	Turcotte, Hamill and Nikolaus	3	2022-03-10 11:20:53.623+07	2022-03-10 11:20:53.623+07
26	Nolan - Kutch	3	2022-03-10 11:20:53.623+07	2022-03-10 11:20:53.623+07
27	Jacobs Group	3	2022-03-10 11:20:53.624+07	2022-03-10 11:20:53.624+07
28	Russel, Carroll and Windler	3	2022-03-10 11:20:53.625+07	2022-03-10 11:20:53.625+07
29	Spinka, Cole and Hammes	3	2022-03-10 11:20:53.625+07	2022-03-10 11:20:53.625+07
30	Dietrich Group	3	2022-03-10 11:20:53.637+07	2022-03-10 11:20:53.637+07
31	Wyman Inc	4	2022-03-10 11:20:53.639+07	2022-03-10 11:20:53.639+07
32	Langosh and Sons	4	2022-03-10 11:20:53.639+07	2022-03-10 11:20:53.639+07
33	Greenholt Inc	4	2022-03-10 11:20:53.64+07	2022-03-10 11:20:53.64+07
34	McLaughlin, Bernhard and Carroll	4	2022-03-10 11:20:53.641+07	2022-03-10 11:20:53.641+07
35	Pouros, Tromp and Gusikowski	4	2022-03-10 11:20:53.642+07	2022-03-10 11:20:53.642+07
36	Zulauf Inc	4	2022-03-10 11:20:53.643+07	2022-03-10 11:20:53.643+07
37	Weissnat - Bayer	4	2022-03-10 11:20:53.643+07	2022-03-10 11:20:53.643+07
38	Farrell, Walsh and Konopelski	4	2022-03-10 11:20:53.645+07	2022-03-10 11:20:53.645+07
39	Brakus, McGlynn and Cole	4	2022-03-10 11:20:53.645+07	2022-03-10 11:20:53.645+07
40	Cole - Boehm	4	2022-03-10 11:20:53.655+07	2022-03-10 11:20:53.655+07
41	Gleason, Kreiger and Ruecker	5	2022-03-10 11:20:53.656+07	2022-03-10 11:20:53.656+07
42	Kertzmann - Bashirian	5	2022-03-10 11:20:53.657+07	2022-03-10 11:20:53.657+07
43	Smitham - Emmerich	5	2022-03-10 11:20:53.657+07	2022-03-10 11:20:53.657+07
44	Jerde, Harris and Fay	5	2022-03-10 11:20:53.658+07	2022-03-10 11:20:53.658+07
45	Anderson LLC	5	2022-03-10 11:20:53.659+07	2022-03-10 11:20:53.659+07
46	Schaden, Jacobs and Monahan	5	2022-03-10 11:20:53.66+07	2022-03-10 11:20:53.66+07
47	Hamill - Stehr	5	2022-03-10 11:20:53.66+07	2022-03-10 11:20:53.66+07
48	Dach, Davis and Jacobson	5	2022-03-10 11:20:53.661+07	2022-03-10 11:20:53.661+07
49	Satterfield, Macejkovic and Bins	5	2022-03-10 11:20:53.662+07	2022-03-10 11:20:53.662+07
50	Blanda Group	5	2022-03-10 11:20:53.663+07	2022-03-10 11:20:53.663+07
\.


--
-- Data for Name: Sekolahs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Sekolahs" (id, nama, "createdAt", "updatedAt") FROM stdin;
1	Will Inc	2022-03-10 11:03:08.853+07	2022-03-10 11:03:08.853+07
2	Stehr, Klein and Tillman	2022-03-10 11:03:09.028+07	2022-03-10 11:03:09.028+07
3	Kuvalis - Gerlach	2022-03-10 11:03:09.032+07	2022-03-10 11:03:09.032+07
4	Blick, Morissette and Bradtke	2022-03-10 11:03:09.034+07	2022-03-10 11:03:09.034+07
5	Barton LLC	2022-03-10 11:03:09.034+07	2022-03-10 11:03:09.034+07
\.


--
-- Data for Name: SequelizeMeta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SequelizeMeta" (name) FROM stdin;
20220309175207-create-sekolah.js
20220309175213-create-kelas.js
20220310014910-create-siswa.js
\.


--
-- Data for Name: Siswas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Siswas" (id, nama, id_kelas, id_sekolah, nilai, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Name: Kelas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Kelas_id_seq"', 50, true);


--
-- Name: Sekolahs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Sekolahs_id_seq"', 5, true);


--
-- Name: Siswas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Siswas_id_seq"', 1, false);


--
-- Name: Kelas Kelas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Kelas"
    ADD CONSTRAINT "Kelas_pkey" PRIMARY KEY (id);


--
-- Name: Sekolahs Sekolahs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Sekolahs"
    ADD CONSTRAINT "Sekolahs_pkey" PRIMARY KEY (id);


--
-- Name: SequelizeMeta SequelizeMeta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SequelizeMeta"
    ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY (name);


--
-- Name: Siswas Siswas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Siswas"
    ADD CONSTRAINT "Siswas_pkey" PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

