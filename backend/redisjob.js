const Queue = require('bull');
const opts = require('./redisconnection')
const sekolahController = require('./controllers/sekolah')

const hitApiQueue = new Queue('sekolah', opts);

hitApiQueue.process(async (job) => {
    try {
  
      const result = await sekolahController.add;
      return Promise.resolve({ result });
    } catch (error) {
      Promise.reject(error);
    }
  });
  
  exports.hitApi = async () => {
    hitApiQueue.add({data: 'test Bull'}, {
        attemps: 1
    });
  
  };