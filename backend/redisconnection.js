const Redis = require('ioredis');
const PORT = '6379';
const HOST = 'redis://127.0.0.1';

const client = new Redis(PORT, HOST);

const opts = {
    createClient(type) {
      switch (type) {
      case 'client':
        return client;
      default:
        return new Redis(PORT, HOST);
      }
    },
  };
  
  exports.default = opts;