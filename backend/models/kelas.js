'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Kelas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Kelas.belongsTo(models.Sekolah, {
        foreignKey: 'id_sekolah',
        as: 'sekolah'
      });
      Kelas.hasMany(models.Siswa, {
        foreignKey: 'id_kelas',
        as: 'siswa'
      });
    }
  }
  Kelas.init({
    nama: DataTypes.STRING,
    id_sekolah: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Kelas',
  });
  return Kelas;
};