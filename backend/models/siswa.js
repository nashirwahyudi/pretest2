'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Siswa extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Siswa.belongsTo(models.Sekolah, {
        foreignKey: 'id_sekolah',
        as: 'sekolah'
      });
      Siswa.belongsTo(models.Kelas, {
        foreignKey: 'id_kelas',
        as: 'kelas'
      });
    }
  }
  Siswa.init({
    nama: DataTypes.STRING,
    id_kelas: DataTypes.INTEGER,
    id_sekolah: DataTypes.INTEGER,
    nilai: DataTypes.FLOAT
  }, {
    sequelize,
    modelName: 'Siswa',
  });
  return Siswa;
};