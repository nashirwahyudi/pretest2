var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var cors = require('./cors');
const kelasController = require('../controllers').kelas;
const sekolahController = require('../controllers').sekolah;
const siswaController = require('../controllers').siswa;
const redisjob = require('./../redisjob');

// const sekolahqueue = require('./../queue/sekolahqueue')

router.use(bodyParser.json());

router.route('/insert-sekolah')
.options(cors.corsWhitelist, (req, res) => { res.sendStatus(200); })
.post(cors.corsWhitelist, sekolahController.add, (req, res, next) => {
    res.json()
})

router.route('/insert-kelas')
.options(cors.corsWhitelist, (req, res) => { res.sendStatus(200); })
.post(cors.corsWhitelist, kelasController.add, (req, res, next) => {
    res.json()
})

router.route('/siswa')
.options(cors.corsWhitelist, (req, res) => { res.sendStatus(200); })
.post(cors.corsWhitelist, siswaController.add, (req, res, next) => {
    res.json()
})
.get(cors.cors, siswaController.getTopTenth, (req, res, next) => {
    res.json()
})

router.route('/insert-sekolah-bull')
.options(cors.corsWhitelist, (req, res) => { res.sendStatus(200); })
.post(cors.corsWhitelist, async (req, res, next) => {
    redisjob.hitApi().then(result => {
        res.json({success: true})
    })
})

module.exports = router;