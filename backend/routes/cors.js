var express = require('express');
var cors = require('cors');
var app = express();

const whitelist = ['http://localhost:8081', 'http://localhost:8080', 'http://localhost:3000'];
var corsOptionsDelegate = (req, callback) => {
    let corsOptions;
    console.log(req.header('Origin'))
    if (whitelist.indexOf(req.header('Origin')) !== -1) {
        corsOptions = { origin: true };
      } else {
        corsOptions = { origin: false };
    } 
    callback(null, corsOptions);
};

exports.cors = cors();
exports.corsWhitelist = cors(corsOptionsDelegate);