const Queue = require('bull');
const sekolahQueue = new Queue('sekolah queue', 'redis://127.0.0.1:6379');
const sekolahProcess = require('./../process/sekolahproccess')

sekolahQueue.process(sekolahProcess);

exports.executeQueue = (data='') => {
    sekolahQueue.add(data, {

    });
}
