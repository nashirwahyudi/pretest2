const Sekolah = require('../models').Sekolah;
const Kelas = require('../models').Kelas;
const faker = require('@faker-js/faker');


const insertSekolahRecursive = (totalSekolah) => {
  return Sekolah.create({
    nama: faker.faker.company.companyName(),
  }).then(sekolah => {
    if (totalSekolah < 4) {
      return insertSekolahRecursive(totalSekolah+1);
    } else {
      return Promise.resolve(true);
    }
  }).catch(error => {
    console.log(error)
})
}

// sekolahQueue.process(async (job, done) => {
//       insertSekolahRecursive(0, 0);
// })

module.exports = {
  add(req, res) {
    return insertSekolahRecursive(0)
        .then((success) => {
            res.setHeader('Content-Type', 'application/json');
            res.statusCode = 200;
            res.json({'message': 'Success adding school!', 'success': success})
        })
        .catch((error) => res.status(50).send({'message': error, 'success': false})); 
  },
};