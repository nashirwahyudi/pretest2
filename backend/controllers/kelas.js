const Sekolah = require('../models').Sekolah;
const Kelas = require('../models').Kelas;
const faker = require('@faker-js/faker');
const sekolah = require('./sekolah');


const insertKelasRecursive = (totalKelas, totalSekolah, sekolahs, quit = false) => {
   if (quit) {
    return Promise.resolve(true); 
   }
    return Kelas.create({
    nama: faker.faker.company.companyName(),
    id_sekolah: sekolahs[totalSekolah].id
  }).then(kelas => {
    if (totalSekolah <= sekolahs.length-1) {
        // console.log(totalSekolah)
        // console.log(totalKelas)
        if (totalKelas < 9) {
            return insertKelasRecursive(totalKelas+1, totalSekolah, sekolahs);
        } else {
            if (totalSekolah == sekolahs.length - 1){            
                return insertKelasRecursive(0, totalSekolah, sekolahs, true);
            }
            return insertKelasRecursive(0, totalSekolah+1, sekolahs);
        }
    } else {
        return Promise.resolve(true); 
    }
  }) .catch(error => {
        console.log('all-seklah:'+sekolahs)
        console.log('sekolah tereakhir'+sekolahs[4])
        console.log('Total Sekolah:'+totalSekolah)
        return Promise.reject(error); 
  })
}

// sekolahQueue.process(async (job, done) => {
//       insertSekolahRecursive(0, 0);
// })
 
module.exports = {
  add(req, res) {
    return Sekolah.findAll({
        attributes: ['id']
    }).then(sekolahs => {
        return sekolahs;
    }).then(sekolahs => {
        return insertKelasRecursive(0, 0, sekolahs)
            .then((success) => {
                res.setHeader('Content-Type', 'application/json');
                res.statusCode = 200;
                res.json({'message': 'Success adding class!', 'success': success})
            })
            .catch((error) => res.status(50).send({'message': error, 'success': false})); 
    }).catch (error => res.status(50).send({'message': error, 'success': false}))
  },
};