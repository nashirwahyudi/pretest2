const sekolah = require('./sekolah.js');
const kelas = require('./kelas.js');
const siswa = require('./siswa.js');

module.exports = {
    sekolah,
    kelas,
    siswa
}