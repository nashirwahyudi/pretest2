const Siswa = require('../models').Siswa;
const Kelas = require('../models').Kelas;
const Sekolah = require('../models').Sekolah;
const faker = require('@faker-js/faker');
const sequelize = require('sequelize')


const insertSiswaRecursive = (totalSiswa, totalKelas, kelas, quit=false) => {
    if(quit) {
        return Promise.resolve(true); 
    }
  return Siswa.create({
    nama: faker.faker.company.companyName(),
    id_sekolah: kelas[totalKelas].id_sekolah,
    id_kelas: kelas[totalKelas].id,
    nilai: faker.faker.datatype.float({
        min: 1,
        max: 100
    })
  }).then(kls => {
    if (totalKelas <= kelas.length-1) {
      if (totalSiswa < 99) {        
        return insertSiswaRecursive(totalSiswa+1, totalKelas, kelas);
        } else {
            if (totalKelas == kelas.length - 1){
                return insertSiswaRecursive(0, totalKelas, kelas, true);
            }
            return insertSiswaRecursive(0, totalKelas+1, kelas);
        }
    }
    return Promise.resolve(true); 
  }) .catch(error => {
      console.log(error)
  })
}
 
// sekolahQueue.process(async (job, done) => { 
//       insertSekolahRecursive(0, 0);
// })
 
module.exports = {
  add(req, res) {
    return Kelas.findAll().then(kelas => {
        return kelas;
    }).then(kelas => {
        return insertSiswaRecursive(0, 0, kelas)
            .then((success) => {
                res.setHeader('Content-Type', 'application/json');
                res.statusCode = 200;
                res.json({'message': 'Success adding student!', 'success': success})
            })
            .catch((error) => res.status(50).send({'message': error, 'success': false})); 
    }).catch (error => res.status(50).send({'message': error, 'success': false}))
  },
  getTopTenth(req, res) {
      return Siswa.findAll({
          attributes: [
                'id', 'nama',
                [sequelize.literal('(RANK() OVER (PARTITION BY id_kelas ORDER BY nilai DESC))'), 'rank kelas'],
                [sequelize.literal('(RANK() OVER (PARTITION BY id_sekolah ORDER BY nilai DESC))'), 'rank sekolah'],
                [sequelize.literal('(RANK() OVER (ORDER BY nilai DESC))'), 'rank'],
          ],
          limit: 10
      }).then(siswas => {
        res.setHeader('Content-Type', 'application/json');
        res.statusCode = 200;
        res.json(siswas)
      })
      .catch((error) => res.status(50).send({'message': error, 'success': false})); 
  }
};